import React, { Component } from 'react';
import './App.css';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';

import { withRouter } from 'react-router-dom';
import { getJwt } from './utils/Utils';

import Routes from './Routes';
import axios from 'axios';

class App extends Component {

  constructor(props) { 
    super(props);
    this.state = { 
      isAuthenticated: false,
      isAuthenticating: true,
      user:{
        firstName:'',
        email:''
      }
    } 
  }

  async componentDidMount(){
    let jwt = getJwt();
    if(jwt){
      axios.get('http://34.218.234.193:3000/api/v1/users/me', { 
        headers: { Authorization: `JWT ${jwt}` }
      })
    // success - set user state
      .then(res => {
          this.setState({
              user: {
                firstName: res.data.firstName,
                email: res.data.email
              },
              isAuthenticated: true
          });   
      })
    // fail - remove session, redirect to login page
      .catch(err => {
          localStorage.removeItem('jwt');
          this.props.history.push('/login');
      })
    }
    // try {
    //   const a = await Auth.currentSession();
    //   console.log('current session');
    //   console.log(a);
    //   this.userHasAuthenticated(true);
    // } catch (e) {
    //   if(e !== 'No current user') {
    //     alert(e);
    //   }
    // }


    this.setState({
      isAuthenticating: false
    })
  }

  userHasAuthenticated = authenticated => { 
    this.setState({ isAuthenticated: authenticated });
  }

  handleLogout = event => {
    localStorage.removeItem('jwt');
    this.userHasAuthenticated(false);
    this.props.history.push('/login');
  }

  setUser = (user) => {
    this.setState({
      user: {
        firstName: user.firstName,
        email: user.email
      }
    })
  }

  render() {
    console.log(this.state);
    const childProps = {
      isAuthenticated: this.state.isAuthenticated, 
      userHasAuthenticated: this.userHasAuthenticated,
      user: this.state.user,
      setUser: this.setUser
    };
       
    return (
      this.state.isAuthenticating ? 
        null :
        (
      <div className="App ">

        <Header 
          childProps = {childProps} 
          handleLogout = {this.handleLogout} 
          userHasAuthenticated = {this.userHasAuthenticated}  
        />

        <Routes childProps={childProps} />
        <Footer />    
      </div>
        )
    );
  }
}

export default withRouter(App);
