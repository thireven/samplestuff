import React from 'react'
import { Switch } from 'react-router-dom';
import Home from './containers/HomePage/Home';
import NotFoundPage from './containers/NotFoundPage/NotFoundPage';
import Login from './containers/LoginPage/Login';
import Signup from './containers/SignupPage/Signup';
import ServicePage from './containers/ServicePage/ServicePage';
import ProjectPage from './containers/ProjectPage/ProjectPage';
import HowToPage from './containers/HowToPage/HowToPage'
import ContactPage from './containers/ContactPage/ContactPage';
import AboutPage from './containers/AboutPage/AboutPage';
import Authenticate from './components/authenticate';

import AppliedRoute from "./components/AppliedRoute";

export default ({ childProps }) => 
<Switch>
    <AppliedRoute path="/" exact component={Home} props={childProps} />
    <AppliedRoute path="/login" exact component={Login} props= {childProps} />
    <AppliedRoute path="/signup" exact component={Signup} props= {childProps} />
    <AppliedRoute path="/howto" exact component={HowToPage} props= {childProps} />
    <AppliedRoute path="/contact" exact component={ContactPage} props= {childProps} />
    <AppliedRoute path="/about" exact component={AboutPage} props= {childProps} />
    <AppliedRoute path="/notfound" component={ NotFoundPage } />
    <Authenticate childProps={childProps}>
        <AppliedRoute path="/project" exact component={ProjectPage} props={childProps} />
        <AppliedRoute path="/service" exact component={ServicePage} props={childProps} />
    </Authenticate>

</Switch>;
