import React, { Component } from 'react'
import { Alert } from 'react-bootstrap';

import "./AlertDismissable.css";

export default class AlertDismissable extends Component {
  constructor(props, context) {
    super(props, context);  
    this.state = {
        show: true
    };
  }
  handleDismiss = () => {
    this.setState({ show: false });
  }

  render() {
    return (
      this.state.show && 
      <Alert className="alertDismissable animated slideInUp" bsStyle="danger" onDismiss={this.handleDismiss}>
        <h4>{this.props.msg}</h4>
      </Alert>
    )
  }
}
