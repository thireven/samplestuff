import React from 'react';
import "./Footer.css";
import { Link } from 'react-router-dom';
import { Grid, Col, Row } from 'react-bootstrap';

export default function Footer() {
  return (
    <Grid className="footer">
      <Row className="show-grid">
        <div className="">
        <Col sm={3} xs={12}>
        <div className="colContent">
          <h3>In4Share</h3>
          <ul>
            <li><Link className="link" to="/">Link 1</Link></li>
            <li><Link className="link" to="/">Link 2</Link></li>
            <li><Link className="link" to="/">Link 3</Link></li>
          </ul>
          </div>
        </Col>
        <Col sm={3} xs={12}>
          <div className="colContent">
          <h3>Company</h3>
          <ul>
          <li><Link className="link" to="/">Link 1</Link></li>
          <li><Link className="link" to="/">Link 2</Link></li>
          <li><Link className="link" to="/">Link 3</Link></li>
          </ul>
          </div>
        </Col>
        <Col sm={3} xs={12}>
          <div className="colContent">
          <h3>Connect</h3>
          <ul>
          <li><Link className="link" to="/">Link 1</Link></li>
          <li><Link className="link" to="/">Link 2</Link></li>
          <li><Link className="link" to="/">Link 3</Link></li>
          </ul>
          </div>
        </Col>
        <Col sm={3} xs={12}>
          <div className="colContent">
          <h3>Policies</h3>
          <ul>
          <li><Link className="link" to="/">Link 1</Link></li>
          <li><Link className="link" to="/">Link 2</Link></li>
          <li><Link className="link" to="/">Link 3</Link></li>
          </ul>
          </div>
        </Col>
      </div>
      </Row>
      <hr />
      <p style={{textAlign:'center',padding:'15px 0'}}><b>&copy; 2018 CompanyName , All Rights Reserved</b></p>
    </Grid>
  )
}
