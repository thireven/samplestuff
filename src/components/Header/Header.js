import React, { Component, Fragment } from 'react'
import { Nav, Navbar, NavItem, NavDropdown, MenuItem } from 'react-bootstrap/lib';
// import { bootstrapUtils } from 'react-bootstrap/lib/utils'
import { Link } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';
import "./Header.css";

export default class Header extends Component {
  render() {
    // console.log(this.props);
    return (
        <Navbar fluid collapseOnSelect style={{height:"150px",paddingTop:"40px",boxSizing:'border-box'}}>

          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/">Logo</Link>
            </Navbar.Brand>
          </Navbar.Header>

          <Nav className='nav'> 
          <NavDropdown eventKey={1} title="Project" id="basic-nav-dropdown1" className="navItem">
            <MenuItem eventKey={2.1} className="navDropDownMenu">
                <Link to="/project">Project</Link>
            </MenuItem>
          </NavDropdown>

          <NavDropdown eventKey={2} title="Service" id="basic-nav-dropdown2" className="navItem">
            <MenuItem eventKey={2.1} className="navDropDownMenu">
                <Link to="/service">Service</Link> 
            </MenuItem>
          </NavDropdown>

          <NavDropdown eventKey={3} title="How To" id="basic-nav-dropdown3" className="navItem">
            <MenuItem eventKey={3.1} className="navDropDownMenu">
              <Link to="/service">How To</Link> 
            </MenuItem>
          </NavDropdown>

          <NavDropdown eventKey={4} title="Contact" id="basic-nav-dropdown4" className="navItem">
            <MenuItem eventKey={4.1} className="navDropDownMenu">
                <Link to="/contact">Contact</Link>
            </MenuItem>
          </NavDropdown>

          <NavDropdown eventKey={5} title="About" id="basic-nav-dropdown5" className="navItem">
            <MenuItem eventKey={5.1} className="navDropDownMenu">
                <Link to="/about">About</Link>
            </MenuItem>
          </NavDropdown>
        </Nav>

        <Nav pullRight>
          { this.props.childProps.isAuthenticated ? 
            <Fragment>
              {this.props.childProps.user.firstName !== '' && <NavItem>Welcome {  this.props.childProps.user.firstName}! </NavItem>}
              <NavItem onClick={this.props.handleLogout}>Logout</NavItem>
            </Fragment>
            :
            <Fragment>
              <LinkContainer to="/signup" style={{color:'red'}}>
                <NavItem>Signup</NavItem> 
              </LinkContainer>
              <LinkContainer to="/login" >
                <NavItem>Login</NavItem>
              </LinkContainer>
            </Fragment>
          }
        </Nav> 
    </Navbar>
    )
  }
}
