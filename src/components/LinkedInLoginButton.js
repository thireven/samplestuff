import React, { Component } from "react";
import { Auth } from "aws-amplify";
import LoaderButton from "./LoaderButton";


const waitForInit = () => {
    return new Promise((res, rej) => {
        const hasINLoaded = () => { 
            if (window.IN) {
                res('loaded'); 
            } else {
                setTimeout(hasINLoaded, 1000);
            }
        hasINLoaded();
        }
    });
}


export default class FacebookButton extends Component { 
    constructor(props) {
        super(props);
        this.state = { 
            isLoading: true
        }; 
    }

    async componentDidMount () {
        // console.log(window.IN);
        this.setState({isLoading: false});
    }

    // handleLinkedInLogin = () => {
    //     this.props.userHasAuthenticated(true);
    // }

    OnLinkedInFrameworkLoad = () =>{
        window.IN.User.authorize(window.IN, 'auth', this.OnLinkedInAuth);
    }
    OnLinkedInAuth = async () => {
        console.log('haha');
        window.IN.API.Profile("me").result(this.ShowProfileData);
        console.log('haha');
        console.log(window.IN.ENV.auth.oauth_token);
    }
    
    ShowProfileData = (profiles) => {
        console.log(profiles);
        var member = profiles.values[0];
        var id=member.id;
        var firstName=member.firstName; 
        var lastName=member.lastName; 
        var photo=member.pictureUrl; 
        var headline=member.headline; 

        console.log('ID : ' + id);
        console.log('Name : ' + firstName + " " + lastName);
  // more code.......
    }

  

    render() { 
        return (
            <div>
            <LoaderButton 
                style={{width:'300px',margin: '10px auto'}}
                block
                bsSize="large" 
                bsStyle="primary" 
                className="LinkedInButton" 
                text="Login with LinkedIn" 
                onClick={this.OnLinkedInFrameworkLoad} 
                disabled={this.state.isLoading}
            />
            </div>
        ); 
    }
 }