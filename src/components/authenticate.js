import React, { Component } from 'react'
import { getJwt } from '../utils/Utils';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

class Authenticate extends Component {

  state = {
      user: undefined
  }

  componentDidMount(){
    // Protect Routes
      if (this.props.location.pathname !== "/project" && this.props.location.pathname !== "/service"){
        this.props.history.push('/notfound');
        return;
      }
    // detect user session
    // if no session, redirect to login page
      
      if(!this.props.childProps.isAuthenticated){
          alert('Please login first!');
          this.props.history.push('/login');
          return;
      }
      // login use JWT
      // axios.get('http://34.218.234.193:3000/api/v1/users/me', { 
      //     headers: { Authorization: `JWT ${jwt}` }
      //   })
      // // success - set user state
      //   .then(res => {
      //       this.setState({
      //           user: res.data
      //       });
      //       // console.log(this.state.user);
      //   })
      // // fail - remove session, redirect to login page
      //   .catch(err => {
      //       localStorage.removeItem('jwt');
      //       this.props.history.push('/login');
      //   })
  }

  render() {
    return (
      <div>
            {this.props.children}
      </div>
    )
  }
}
export default withRouter(Authenticate);
