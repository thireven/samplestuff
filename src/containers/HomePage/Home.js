import React, { Component } from 'react'
import { FormGroup, FormControl, Button } from 'react-bootstrap';

import './Home.css';

export default class Home extends Component {

  state = {
    searchText:''
  }

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state.searchText);
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  render() {
    return (
      <div className="home">
        <div className="lander">
            <h1 className="caption">In4Share</h1>
            <h3>Provide Real IT Service Search</h3>
            <form onSubmit={this.handleSubmit}>
              <FormGroup controlId="text" bsSize="large">
                <FormControl
                  autoFocus 
                  placeholder="Search For IT Service..." 
                  type="text" 
                  name="searchText"
                  value={this.state.searchText} 
                  onChange={this.handleChange}
              />
                <Button className="submitButton" type="submit">Search</Button>
              </FormGroup>
            </form>
        </div>
    </div>
    )
  }
}
