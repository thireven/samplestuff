import React, { Component } from "react";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from '../../components/LoaderButton';
import LinkedInLoginButton from '../../components/LinkedInLoginButton';
import AlertDismissable from '../../components/AlertDismissable/AlertDismissable';

import { validatePassword } from '../../utils/Utils';

import axios from 'axios';

import "./Login.css";

export default class Login extends Component { 
    constructor(props) {
        super(props);
        this.state = { 
            email: "", 
            password: "",
            err: "",
            firstName:''
        }; 
    }

    componentWillMount(){
        if(this.props.isAuthenticated) 
           this.props.history.push('/');
    }

    validateForm() {
        return this.state.email.length > 3 && this.state.password.length > 3; 
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value 
        });
    }

    handleSubmit = async e => {
        e.preventDefault();
        this.setState({
            err: ''
        })
        if(!validatePassword(this.state.password)) {
            this.setState({
                // set default password length in utils
                err: ' Password must contain at least five characters, at least one letter and one number.'
            });
            return;
        }
        try {
            const res = await axios.post("http://34.218.234.193:3000/api/v1/users/login",{
                email: this.state.email,
                password: this.state.password
            })
            localStorage.setItem('jwt',res.data.jwt);
            this.props.userHasAuthenticated(true);
            this.props.history.push('/');
            this.props.setUser(res.data);
        }
        catch (err) {
            let msg = err.message;
            if(msg.includes('400'))
                this.setState({
                    err: "Password and Email must be valid. Please try again!"
                })
            else if (msg.includes('401'))
            this.setState({
                err: 'Password and Email must Match. Please try again!'
            })
            else 
            this.setState({
                err: "Please try again later!"
            })
        };
    }


    render() { 
        return (
            <div className="Login">
                <LinkedInLoginButton />
                <hr/>

                {this.state.err && <AlertDismissable msg={this.state.err} />}
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                            autoFocus placeholder="Enter your email" type="email" value={this.state.email} onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel> 
                        <FormControl placeholder="Enter your password" type="password" value={this.state.password} onChange={this.handleChange} 
                    /> 
                    </FormGroup> 
                    <LoaderButton  
                        block 
                        bsSize="large" 
                        isLoading={this.state.isLoading}
                        disabled={!this.validateForm()} 
                        type="submit"
                        text="Login"
                        loadingText="Logging in..."
                    >
                    Login
                    </LoaderButton>
                </form>
            </div> 
        );
    }
}

 