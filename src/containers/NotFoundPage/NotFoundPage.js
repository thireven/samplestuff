import React from 'react'
import {Button} from 'react-bootstrap';
import './NotFoundPage.css';

export default function NotFound(props) {

  

  return (
    <div className="NotFound">
      <h3>Sorry, page not found</h3>
      <Button onClick={() => props.history.push('/')} style={{marginTop:"30px"}}>Go to Homepage</Button> 
    </div>
  )
}
