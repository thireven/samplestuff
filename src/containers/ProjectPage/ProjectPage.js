import React, { Component } from 'react'

export default class ProjectPage extends Component {

  componentDidMount(){
    if (!this.props.isAuthenticated) {
      this.props.history.push('/login')
    }
  }

  render() {
    return (
      <div>
        Your Project Profile
      </div>
    )
  }
}
