import React, { Component } from "react";
import {  FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../../components/LoaderButton";
import AlertDismissable from '../../components/AlertDismissable/AlertDismissable';
import {validatePassword} from '../../utils/Utils';

import "./Signup.css";
import axios from 'axios';

export default class Signup extends Component { 
    constructor(props) {
        super(props);
        this.state = { 
            email: "",
            password: "", 
            confirmPassword: "",
            confirmationCode: "",
            err:[],
            // if need code auth, use newUser
            // newUser: null,
            // if need get loading status
            // isLoading: false
        }; 
    }

    validateForm() { 
        return (
            this.state.email.length > 0 && 
            this.state.password.length > 0 &&
            this.state.password === this.state.confirmPassword 
        );
    }

    validateConfirmationForm() {
        return this.state.confirmationCode.length > 0;
    }

    handleChange = event => { 
        this.setState({
            [event.target.id]: event.target.value 
        });
    }

    handleSubmit = async event => {
        event.preventDefault();
        this.setState({
            err: ''
        })
        if(!validatePassword(this.state.password)) {
            this.setState({
        // set default password length in utils
                err: ' Password must contain at least five characters, at least one letter and one number.'
            });
        // stop sign up process
            return;
        }
        // handle loading status
        //this.setState({ isLoading: true });

        // Sign up start here
        try {
            await axios.post('http://34.218.234.193:3000/api/v1/users',{
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                email: this.state.email,
                password: this.state.password
            });
        } catch (err) {
            let msg = err.message;
            console.log(msg);
            if(msg.includes('unique'))
                this.setState({
                    err: "Email address has been registerd. Please sign in!"
                })
            else if (msg.includes('valid'))
            this.setState({
                err: 'Password and Email should be valid. Please try again!'
            })
            else 
            this.setState({
                err: "Please try again later!"
            })
            return;
        };
        // Login start here
        try {
            const res = await axios.post("http://34.218.234.193:3000/api/v1/users/login",{
                email: this.state.email,
                password: this.state.password
            })
            localStorage.setItem('jwt',res.data.jwt);
            this.props.userHasAuthenticated(true);
            this.props.history.push('/');
            this.props.childProps.setUser(res.data);
        }
        catch (err) {
            let msg = err.message;
            if(msg.includes('400'))
                this.setState({
                    err: "Password and Email must be valid. Please try again!"
                })
            else if (msg.includes('401'))
            this.setState({
                err: 'Password and Email must Match. Please try again!'
            })
            else 
            this.setState({
                err: "Please try again later!"
            })
        };
        // handle loading status
        // this.setState({ isLoading: false }); 
    }
       
    
    // handleConfirmationSubmit = async event => { 
    //     event.preventDefault();
    //     this.setState({ isLoading: true });
    //     try {
    //         await Auth.confirmSignUp(this.state.email, this.state.confirmationCode);
    //         await Auth.signIn(this.state.email, this.state.password);
    //         this.props.userHasAuthenticated(true);
    //         this.props.history.push("/"); 
    //     } catch (e) {
    //         alert(e.message);
    //         this.setState({ isLoading: false }); 
    //     }
    // }

    // renderConfirmationForm(){ 
    //     return (
    //         <form onSubmit={this.handleConfirmationSubmit}> 
    //             <FormGroup controlId="confirmationCode" bsSize="large">
    //                 <ControlLabel>Confirmation Code</ControlLabel> 
    //                 <FormControl 
    //                     autoFocus 
    //                     type="text"
    //                     value={this.state.confirmationcode}
    //                     onChange={this.handleChange}
    //                 />
    //                 <HelpBlock>Please check your email for the code.</HelpBlock> 
    //             </FormGroup>
    //             <LoaderButton
    //                 block
    //                 bsSize="large" 
    //                 disabled={!this.validateConfirmationForm()} type="submit" isLoading={this.state.isLoading} text="Verify"
    //                 loadingText="Verifying..."
    //             /> 
    //         </form>
    //     ); 
    // }

    renderForm() { 
        return (
            <form onSubmit={this.handleSubmit}>
                <FormGroup controlId="firstName" bsSize="large">
                    <ControlLabel>First Name</ControlLabel> 
                    <FormControl
                    autoFocus
                    type="text" 
                    value={this.state.firstName} 
                    onChange={this.handleChange}
                    />
                </FormGroup>
                <FormGroup controlId="lastName" bsSize="large">
                    <ControlLabel>Last Name</ControlLabel> 
                    <FormControl
                    type="text" 
                    value={this.state.lastName} 
                    onChange={this.handleChange}
                    />
                </FormGroup>
                <FormGroup controlId="email" bsSize="large">
                    <ControlLabel>Email</ControlLabel> 
                    <FormControl
                    type="email" 
                    value={this.state.email} 
                    onChange={this.handleChange}
                    />
                 </FormGroup>
                <FormGroup controlId="password" bsSize="large">
                    <ControlLabel>Password</ControlLabel> 
                    <FormControl
                        value={this.state.password} 
                        onChange={this.handleChange} 
                        type="password"
                    />
                </FormGroup>
                <FormGroup controlId="confirmPassword" bsSize="large">
                    <ControlLabel>Confirm Password</ControlLabel> 
                    <FormControl
                        value={this.state.confirmPassword} 
                        onChange={this.handleChange} 
                        type="password"
                    /> 
                </FormGroup> 
                <LoaderButton
                    block
                    bsSize="large" 
                    disabled={!this.validateForm()} 
                    type="submit" 
                    isLoading={this.state.isLoading} 
                    text="Signup" 
                    loadingText="Signing up..."
                /> 
            </form>
        ); 
    }

    render() { 
        // console.log(this.state);
        return (
            <div className="Signup">
                {this.state.err.length!==0 && <AlertDismissable msg={this.state.err} />}
                {this.renderForm()}
            </div>
        ); 
    }
}


// {this.state.newUser === null
//     ? this.renderForm()
//     : this.renderConfirmationForm()} 

