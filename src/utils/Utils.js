export const getJwt = () => localStorage.getItem('jwt');

export const validatePassword = (w) => {
    let pattern = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/;
    return pattern.test(w);
}